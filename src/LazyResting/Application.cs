﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;

namespace LazyResting
{
    public interface IApplication
    {
        Task Run(ProgramArgs args);
    }

    public class Application : IApplication
    {
        private readonly ITemplateRenderer _templateRenderer;
        private readonly IDatabaseInspector _databaseInspector;
        private readonly ITemplateProvider _templateProvider;

        public Application(ITemplateRenderer templateRenderer, IDatabaseInspector databaseInspector,
            ITemplateProvider templateProvider)
        {
            _templateRenderer = templateRenderer ?? throw new ArgumentNullException(nameof(templateRenderer));
            _databaseInspector = databaseInspector ?? throw new ArgumentNullException(nameof(databaseInspector));
            _templateProvider = templateProvider ?? throw new ArgumentNullException(nameof(templateProvider));
        }
        
        public Task Run(ProgramArgs args)
        {
            switch (args.Subcommand)
            {
                case Subcommand.RenderTemplate:
                    return Render(args);
                case Subcommand.ShowInfo:
                    return ShowInfo(args);
                default:
                    return Task.CompletedTask;
            }
        }

        private async Task Render(ProgramArgs args)
        {
            var tablesTask = _databaseInspector.ListTables(args.Tables);
            var templates = _templateProvider.GetTemplates(args.Templates);
            await Task.WhenAll(tablesTask, templates);

            foreach (var template in templates.Result)
            {
                foreach (var table in tablesTask.Result)
                {
                    var renderedRepo = _templateRenderer.Render(template, table, args);
                    Console.WriteLine(renderedRepo);
                }
            }
        }

        private async Task ShowInfo(ProgramArgs args)
        {
            new ConsoleTable(title: "program arguments", rows: ImmutableArray.Create(
                new ConsoleTableRow("connection string", args.ConnString),
                new ConsoleTableRow("tables", args.Tables.Length == 0 ? "<all>" : string.Join(", ", args.Tables)),
                new ConsoleTableRow("templates", string.Join(", ", args.Templates)),
                new ConsoleTableRow("project name", args.ProjectName)
            )).Print();
            
            var tables = await _databaseInspector.ListTables(args.Tables);

            foreach (var table in tables)
            {
                new ConsoleTable(
                    title: table.Name.Original, 
                    rows: table.Columns.Select(c => new ConsoleTableRow(
                        columnOne: c.Name.Original,
                        columnTwo: string.Join(" ", GetSqlTypeInfo(c)),
                        columnThree: c.CrlType.Name
                    ))
                    .ToImmutableArray()
                ).Print();
            }
        }

        private static IEnumerable<string> GetSqlTypeInfo(ColumnInfo columnInfo)
        {
            yield return columnInfo.SqlType.ToUpper();
            yield return columnInfo.IsNullable ? "NULL" : "NOT NULL";
            if (columnInfo.IsPK) yield return "PRIMARY KEY";
        }

        private class ConsoleTable
        {
            internal string Title { get; }
            internal ImmutableArray<ConsoleTableRow> Rows { get; }

            internal ConsoleTable(string title, ImmutableArray<ConsoleTableRow> rows)
            {
                Title = title;
                Rows = rows;
            }

            internal void Print()
            {
                Console.WriteLine();
                Console.WriteLine(Title);
                Console.WriteLine(new String('-', Title.Length));

                foreach (var row in Rows)
                {
                    row.Print();
                }
            }
        }

        private class ConsoleTableRow
        {
            private const int FirstColumnWidth = -20;
            private const int SecondColumnWidth = -30;
            
            internal string ColumnOne { get; }
            internal string ColumnTwo { get; }
            internal string ColumnThree { get; }

            internal ConsoleTableRow(string columnOne, string columnTwo, string columnThree = null)
            {
                ColumnOne = columnOne;
                ColumnTwo = columnTwo;
                ColumnThree = columnThree;
            }

            internal void Print()
            {
                Console.WriteLine($"{ColumnOne,FirstColumnWidth}{ColumnTwo,SecondColumnWidth}{ColumnThree}");                
            }
        }
    }
}