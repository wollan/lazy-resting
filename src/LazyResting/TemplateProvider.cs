﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Controllers;
using ProjectX.Domain.Model;
using ProjectX.Domain.Services;
using ProjectX.Messages;
using ProjectX.Repositories;

namespace LazyResting
{
    public interface ITemplateProvider
    {
        Task<string> GetTemplate(Template template);
    }

    public class TemplateProvider : ITemplateProvider
    {
        public Task<string> GetTemplate(Template template)
        {
            switch (template)
            {
                case Template.Repository:
                    return GetTemplate<TableXRepository>();
                case Template.DomainModel:
                    return GetTemplate<TableXDomainModel>();
                case Template.DtoModel:
                    return GetTemplate<TableXDTO>();
                case Template.Service:
                    return GetTemplate<TableXService>();
                case Template.Controller:
                    return GetTemplate<TableXController>();
                default:
                    throw new ArgumentOutOfRangeException(nameof(template), template, null);
            }
        }

        private async Task<string> GetTemplate<T>()
        {
            var type = typeof(T);
            var suffixToMatch = $".{type.Name}.cs";
            var all = type.Assembly.GetManifestResourceNames();
            var name = all.SingleOrDefault(n => n.EndsWith(suffixToMatch, StringComparison.InvariantCultureIgnoreCase))
               ?? throw new Exception("could not find template: " + type.Name);
            
            using (var stream = type.Assembly.GetManifestResourceStream(name))
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    var content = await reader.ReadToEndAsync();
                    return content;
                }
            }
        }
    }

    public static class TemplateProviderExtensions
    {
        public static async Task<ImmutableArray<string>> GetTemplates(this ITemplateProvider provider,
            IEnumerable<Template> templates) =>
            (await Task.WhenAll(templates.Select(provider.GetTemplate))).ToImmutableArray();
    }
}