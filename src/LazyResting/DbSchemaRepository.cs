﻿using System;
using System.Collections.Immutable;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

// ReSharper disable InconsistentNaming

namespace LazyResting
{
    public interface IDbSchemaRepository
    {
        Task<ImmutableArray<TableInfo>> ListTables();
    }
    
    public class DbSchemaRepository : IDbSchemaRepository
    {
        private readonly string _connectionString;

        public DbSchemaRepository(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(connectionString));
            
            _connectionString = connectionString;
        }
        
        public Task<ImmutableArray<TableInfo>> ListTables()
        {
            return OpenConnection(async connection =>
            {
                var tableQuery = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE'";
                var tableNames = (await connection.QueryAsync<string>(sql: tableQuery)).ToImmutableArray();
                
                var columnQuery = @"
                    SELECT c.TABLE_NAME, c.COLUMN_NAME, c.DATA_TYPE, c.IS_NULLABLE, c.ORDINAL_POSITION,
                      (CASE WHEN pk.COLUMN_NAME IS NULL THEN 0 ELSE 1 END) AS IS_PK
                    FROM INFORMATION_SCHEMA.COLUMNS c
                    LEFT JOIN (
                        SELECT kc.TABLE_NAME, kc.COLUMN_NAME 
                        FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE kc
                        INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc ON tc.CONSTRAINT_NAME = kc.CONSTRAINT_NAME
                        WHERE CONSTRAINT_TYPE = 'PRIMARY KEY'
                    ) pk ON pk.TABLE_NAME = c.TABLE_NAME AND pk.COLUMN_NAME = c.COLUMN_NAME
                ";
                
                var columnResults = (await connection.QueryAsync<ColumnResult>(sql: columnQuery))
                    .ToLookup(c => c.TABLE_NAME);

                var tablesInfos = tableNames
                    .Select(n => new TableInfo(
                        name: n,
                        columns: columnResults[n]
                            .OrderBy(c => c.ORDINAL_POSITION)
                            .Select(c => new ColumnInfo(
                                name: c.COLUMN_NAME,
                                sqlType: c.DATA_TYPE,
                                isPk: c.IS_PK,
                                isNullable: c.IS_NULLABLE == "YES"))
                            .ToImmutableArray()))
                    .ToImmutableArray();

                return tablesInfos;
            });

        }

        public class ColumnResult
        {
            public string TABLE_NAME { get; set; }
            public string COLUMN_NAME { get; set; }
            public string DATA_TYPE { get; set; }
            public string IS_NULLABLE { get; set; }
            public int ORDINAL_POSITION { get; set; }
            public bool IS_PK { get; set; }
        }
        
        private async Task<T> OpenConnection<T>(Func<SqlConnection, Task<T>> func)
        {
            if (func == null) throw new ArgumentNullException(nameof(func));
            
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                return await func(connection);
            }
        }
    }

}