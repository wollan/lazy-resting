﻿using System;
using System.Collections.Immutable;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectX.Domain.Services;
using ProjectX.Messages;

namespace ProjectX.Controllers
{
    [Route("api/v1/tableXs")]
    public class TableXController : Controller
    {
        private readonly ITableXService _tableXService;

        public TableXController(ITableXService tableXService)
        {
            _tableXService = tableXService ?? throw new ArgumentNullException(nameof(tableXService));
        }

        [HttpGet("{primaryKeyX}")]
        public async Task<IActionResult> Get(PrimaryKeyX primaryKeyX)
        {
            try
            {
                var tableX = await _tableXService.GetByPrimaryKey(primaryKeyX);
                return Ok(tableX);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get(int offset, int limit)
        {
            try
            {
                var tableXs = await _tableXService.FindAll(offset: offset, limit: limit);
                return Ok(tableXs);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public Task<IActionResult> Post([FromBody] TableXDTO tableX) => 
            Post(ImmutableArray.Create(tableX));

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ImmutableArray<TableXDTO> tableXs)
        {
            try
            {
                await _tableXService.CreateTableXs(tableXs);
                return Ok();
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("{primaryKeyX}")]
        public Task<IActionResult> Put(PrimaryKeyX primaryKeyX, [FromBody] TableXDTO tableX) =>
            Put(ImmutableArray.Create(tableX));

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] ImmutableArray<TableXDTO> tableXs)
        {
            try
            {
                await _tableXService.PutTableXs(tableXs);
                return Ok();
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPatch("{primaryKeyX}")]
        public Task<IActionResult> Patch(PrimaryKeyX primaryKeyX, [FromBody] TableXDTO tableX) =>
            Patch(ImmutableArray.Create(tableX));

        [HttpPatch]
        public async Task<IActionResult> Patch([FromBody] ImmutableArray<TableXDTO> tableXs)
        {
            try
            {
                await _tableXService.UpdateTableXs(tableXs);
                return Ok();
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{primaryKeyX}")]
        public Task<IActionResult> Delete(PrimaryKeyX primaryKeyX) =>
            Delete(ImmutableArray.Create(primaryKeyX));

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] ImmutableArray<PrimaryKeyX> primaryKeyXs)
        {
            try
            {
                await _tableXService.RemoveTableXs(primaryKeyXs);
                return Ok();
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}