﻿using System;

namespace ProjectX.Messages
{
    public class TableXDTO
    {
        public PrimaryKeyX PrimaryKey { get; }

        public TableXDTO(PrimaryKeyX primaryKey)
        {
            PrimaryKey = primaryKey;
        }
    }
}
