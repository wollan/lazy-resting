﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using ProjectX.Domain.Model;

namespace ProjectX.Repositories
{
    public interface ITableXRepository
    {
        Task<TableXDomainModel> GetByPrimaryKey(PrimaryKeyX primaryKey);

        Task<ImmutableArray<TableXDomainModel>> FindAll(int offset, int limit);

        Task InsertAll(IEnumerable<TableXDomainModel> tableXs);

        Task UpsertAll(IEnumerable<TableXDomainModel> tableXs);
        
        Task UpdateAll(IEnumerable<TableXDomainModel> tableXs);

        Task DeleteByPrimaryKeys(IEnumerable<PrimaryKeyX> primaryKeys);
    }
    
    public class TableXRepository : ITableXRepository
    {
        private readonly string _connectionString;

        public TableXRepository(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(connectionString));
            
            _connectionString = connectionString;
        }

        public Task<TableXDomainModel> GetByPrimaryKey(PrimaryKeyX primaryKey)
        {
            return OpenConnection(async connection =>
            {
                var query = "SELECT * FROM TableX WHERE PrimaryKey = @primaryKey";

                var tableX = await connection.QueryFirstAsync<TableXDomainModel>(
                    sql: query, param: new { primaryKey = primaryKey });

                return tableX;
            });
        }

        public Task<ImmutableArray<TableXDomainModel>> FindAll(int offset, int limit)
        {
            return OpenConnection(async connection =>
            {
                var query = @"
                    SELECT * FROM TableX 
                    ORDER BY PrimaryKey
                    OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY
                ";

                var tableXs = (await connection.QueryAsync<TableXDomainModel>(
                    sql: query, param: new { offset = offset, limit = limit }))
                    .ToImmutableArray();

                return tableXs;
            });
        }

        public Task InsertAll(IEnumerable<TableXDomainModel> tableXs)
        {
            throw new NotImplementedException();
        }

        public Task UpsertAll(IEnumerable<TableXDomainModel> tableXs)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAll(IEnumerable<TableXDomainModel> tableXs)
        {
            throw new NotImplementedException();
        }

        public Task DeleteByPrimaryKeys(IEnumerable<PrimaryKeyX> primaryKeys)
        {
            throw new NotImplementedException();
        }

        private async Task<T> OpenConnection<T>(Func<SqlConnection, Task<T>> func)
        {
            if (func == null) throw new ArgumentNullException(nameof(func));
            
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                return await func(connection);
            }
        }
    }
}
