﻿using System;

namespace ProjectX.Domain.Model
{
    public class TableXDomainModel
    {
        public PrimaryKeyX PrimaryKey { get; }

        public TableXDomainModel(PrimaryKeyX primaryKey)
        {
            PrimaryKey = primaryKey;
        }
        
        public static TableXDomainModel Create(PrimaryKeyX primaryKey) => 
            new TableXDomainModel(primaryKey: primaryKey);
    }
}
