﻿using System;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using ProjectX.Domain.Model;
using ProjectX.Repositories;
using TableXDTODto = ProjectX.Messages.TableXDTO;

namespace ProjectX.Domain.Services
{
    public interface ITableXService
    {
        Task<TableXDTODto> GetByPrimaryKey(PrimaryKeyX primaryKey);

        Task<ImmutableArray<TableXDTODto>> FindAll(int offset, int limit);

        Task CreateTableXs(ImmutableArray<TableXDTODto> tableXs);

        Task PutTableXs(ImmutableArray<TableXDTODto> tableXs);

        Task UpdateTableXs(ImmutableArray<TableXDTODto> tableXs);

        Task RemoveTableXs(ImmutableArray<PrimaryKeyX> primaryKeyXs);
    }
    
    public class TableXService : ITableXService
    {
        private readonly ITableXRepository _tableXRepository;

        public TableXService(ITableXRepository tableXRepository)
        {
            _tableXRepository = tableXRepository ?? throw new ArgumentNullException(nameof(tableXRepository));
        }
        
        public async Task<TableXDTODto> GetByPrimaryKey(PrimaryKeyX primaryKey)
        {
            if (primaryKey == null) throw new ArgumentNullException(nameof(primaryKey));

            var tableX = await _tableXRepository.GetByPrimaryKey(primaryKey);

            return ToDto(tableX);
        }

        public async Task<ImmutableArray<TableXDTODto>> FindAll(int offset, int limit)
        {
            if (offset < 0) 
                throw new ArgumentOutOfRangeException(nameof(offset), offset, "offset must be zero or higher");
            if (limit <= 0 || limit > 1000) 
                throw new ArgumentOutOfRangeException(nameof(limit), limit, "limit must be between one and 1000");

            var tableXs = await _tableXRepository.FindAll(offset: offset, limit: limit);

            return tableXs.Select(ToDto).ToImmutableArray();
        }

        public Task CreateTableXs(ImmutableArray<TableXDTODto> tableXs)
        {
            if (tableXs == default || tableXs.IsEmpty)
            {
                throw new ArgumentException("Cannot create zero tableXs!");
            }
            
            return _tableXRepository.InsertAll(tableXs.Select(FromDto));
        }

        public Task PutTableXs(ImmutableArray<TableXDTODto> tableXs)
        {
            if (tableXs == default || tableXs.IsEmpty)
            {
                throw new ArgumentException("Cannot put zero tableXs!");
            }

            return _tableXRepository.UpsertAll(tableXs.Select(FromDto));
        }

        public Task UpdateTableXs(ImmutableArray<TableXDTODto> tableXs)
        {
            if (tableXs == default || tableXs.IsEmpty)
            {
                throw new ArgumentException("Cannot update zero tableXs!");
            }
            
            return _tableXRepository.UpdateAll(tableXs.Select(FromDto));
        }

        public Task RemoveTableXs(ImmutableArray<PrimaryKeyX> primaryKeyXs)
        {
            if (primaryKeyXs == default || primaryKeyXs.IsEmpty)
            {
                throw new ArgumentException("Cannot remove zero tableX primaryKeys!");
            }
            
            return _tableXRepository.DeleteByPrimaryKeys(primaryKeyXs);
        }

        private static TableXDTODto ToDto(TableXDomainModel tableX) => new TableXDTODto(
            primaryKey: tableX.PrimaryKey
        );
        
        private static TableXDomainModel FromDto(TableXDTODto tableX) => new TableXDomainModel(
            primaryKey: tableX.PrimaryKey
        );
    }
}