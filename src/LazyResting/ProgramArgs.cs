﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using System.IO;
using System.Linq;
using CommandLine;

namespace LazyResting
{
    public class ProgramArgs
    {
        public Subcommand Subcommand { get; }
        public string ConnString { get; }
        public ImmutableArray<string> Tables { get; } // empty means use all tables
        public ImmutableArray<Template> Templates { get; }
        public string ProjectName { get; }

        public ProgramArgs(
            string connString,
            ImmutableArray<string> tables,
            Subcommand subcommand,
            ImmutableArray<Template> templates, 
            string projectName)
        {
            if (string.IsNullOrWhiteSpace(connString))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(connString));
            if (!Enum.IsDefined(typeof(Subcommand), subcommand))
                throw new InvalidEnumArgumentException(nameof(subcommand), (int) subcommand, typeof(Subcommand));
            if (string.IsNullOrWhiteSpace(projectName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(projectName));
            if (templates == default)
                throw new ArgumentException("Value cannot be default", nameof(templates));
            if (tables == default)
                throw new ArgumentException("Value cannot be default", nameof(tables));
            foreach (var template in templates)
            {
                if (!Enum.IsDefined(typeof(Template), template))
                    throw new InvalidEnumArgumentException(nameof(template), (int) template, typeof(Template));                
            }

            ConnString = connString;
            Subcommand = subcommand;
            ProjectName = projectName;
            Templates = templates.IsEmpty
                ? Enum.GetValues(typeof(Template)).Cast<Template>().ToImmutableArray()
                : templates;
            Tables = tables;
        }

        public static ProgramArgs ParseOrExit(string[] args)
        {
            return Parser.Default.ParseArguments<RenderOptions, InfoOptions>(args)
                .MapResult(
                    parsedFunc1: (RenderOptions o) => Create(o, Subcommand.RenderTemplate),
                    parsedFunc2: (InfoOptions o) => Create(o, Subcommand.ShowInfo),
                    notParsedFunc: errors =>
                    {
                        Environment.Exit(1);
                        return null;
                    });
        }

        private static ProgramArgs Create(Options options, Subcommand subcommand)
        {
            try
            {
                return new ProgramArgs(
                    connString: ParseConnString(options),
                    subcommand: subcommand,
                    templates: ParseTemplates(options.Templates),
                    projectName: GetProjectName(options.ProjectName),
                    tables: options.Tables?.ToImmutableArray() ?? ImmutableArray<string>.Empty);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("Invalid argument format: " + e.Message);
                Environment.Exit(1);
                throw;
            }
        }

        private static string ParseConnString(Options options)
        {
            if (options.DatabaseName == null) return options.ConnectionString;
            
            var database = "Database=";
            var connStr = options.ConnectionString;
            int dbKeyIndex = connStr.IndexOf(database, StringComparison.Ordinal);
            var dbValueIndex = dbKeyIndex + database.Length;
            int nextSemicolonIndex = dbValueIndex + connStr.Substring(dbValueIndex).IndexOf(';');
            return connStr.Substring(0, dbValueIndex) + 
                   options.DatabaseName +
                   connStr.Substring(nextSemicolonIndex);
        }

        private static string GetProjectName(string optionsProjectName)
        {
            return string.IsNullOrWhiteSpace(optionsProjectName) 
                ? new DirectoryInfo(Environment.CurrentDirectory).Name 
                : optionsProjectName;
        }

        private static ImmutableArray<Template> ParseTemplates(IEnumerable<string> optionsTemplates)
        {
            if (optionsTemplates == null || !optionsTemplates.Any())
            {
                return ImmutableArray<Template>.Empty;
            }

            var templates = Enum.GetValues(typeof(Template))
                .Cast<Template>()
                .Select(t => (name: t.ToString().ToLower(), value: t))
                .ToImmutableArray();

            return optionsTemplates.Select(t => 
            {
                var match = templates.FirstOrDefault(t2 => t2.name.Contains(t.ToLower()));
                if (match.Equals(default))
                {
                    throw new ArgumentException($"Template '{t}' does not match any of " +
                                                $"{string.Join(", ", templates.Select(t2 => t2.value))}");
                }
                return match.value;
            }).ToImmutableArray();
        }

        private class Options
        {
            [Option(shortName: 'c', longName: "connectionString", 
                Required = false,
                Default = "Server=localhost;Database=DatabaseX;User Id=SA;Password=ApaBepa_42;", 
                HelpText = "The sql database connection string")]
            public string ConnectionString { get; set; }

            [Option(shortName: 'd', longName: "database",
                Required = false,
                HelpText = "Override the database name in connection string.")]
            public string DatabaseName { get; set; }

            [Option(shortName: 'p', longName: "project",
                Required = false,
                HelpText = "Set the project name, defaults to current directory name.")]
            public string ProjectName { get; set; }

            [Option(shortName: 't', longName: "tables",
                Required = false,
                Separator = ',',
                HelpText = "List all tables to use, separated by comma. Defaults to all tables.")]
            public IEnumerable<string> Tables { get; set; }
            
            [Option(shortName: 'l', longName: "templates",
                Required = false,
                Separator = ',',
                HelpText = "List all templates to use, separated by comma. Defaults to all templates.")]
            public IEnumerable<string> Templates { get; set; }
        }
        
        [Verb("render", HelpText = "Generate C# code based on database schema")]
        private class RenderOptions : Options 
        {
        }

        [Verb("info", HelpText = "Show runtime info like list of all matching tables")]
        private class InfoOptions : Options 
        {
        }
    }

    public enum Subcommand
    {
        RenderTemplate = 1,
        ShowInfo
    }

    public enum Template
    {
        Repository,
        DomainModel,
        Service,
        Controller,
        DtoModel,
    }
}