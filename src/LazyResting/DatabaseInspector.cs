﻿using System;
using System.Collections.Immutable;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

// ReSharper disable InconsistentNaming

namespace LazyResting
{
    public interface IDatabaseInspector
    {
        Task<ImmutableArray<TableInfo>> ListTables(ImmutableArray<string> tablesToShow);
    }
    
    public class DatabaseInspector : IDatabaseInspector
    {
        private readonly IDbSchemaRepository _dbSchemaRepository;

        public DatabaseInspector(IDbSchemaRepository dbSchemaRepository)
        {
            _dbSchemaRepository = dbSchemaRepository ?? throw new ArgumentNullException(nameof(dbSchemaRepository));
        }
        
        public async Task<ImmutableArray<TableInfo>> ListTables(ImmutableArray<string> tablesToShow)
        {
            var result = await _dbSchemaRepository.ListTables();
            if (tablesToShow == ImmutableArray<string>.Empty)
            {
                return result;
            }
            
            var tableSetToShow = tablesToShow.ToImmutableHashSet();
            return result.Where(t => tableSetToShow.Contains(t.Name.Original)).ToImmutableArray();
        }
    }

    public class TableInfo
    {
        public SqlName Name { get; }
        public ImmutableArray<ColumnInfo> Columns { get; }
        public ColumnInfo PrimaryKey { get; }

        public TableInfo(ImmutableArray<ColumnInfo> columns, string name)
        {
            Name = new SqlName(name);
            Columns = columns;
            PrimaryKey = Columns.SingleOrDefault(c => c.IsPK) ?? throw new ArgumentException("no pk found!");
        }
    }

    public class ColumnInfo
    {
        public SqlName Name { get; }
        public string SqlType { get; }
        public bool IsPK { get; }
        public bool IsNullable { get; }
        public CrlType CrlType { get; }
        
        public ColumnInfo(string name, string sqlType, bool isPk, bool isNullable)
        {
            Name = new SqlName(name);
            SqlType = sqlType;
            IsPK = isPk;
            IsNullable = isNullable;
            CrlType = CrlType.FromSqlType(sqlType, isNullable);
        }
    }

    public class CrlType
    {
        public static CrlType String { get; } = new CrlType("string", "null", false);
        public static CrlType Bool { get; } = new CrlType("bool", "false", false);
        
        public static CrlType Long { get; } = new CrlType("long", "0L", false);
        public static CrlType Int { get; } = new CrlType("int", "0", false);
        public static CrlType Short { get; } = new CrlType("short", "default", false); //
        
        public static CrlType DateTime { get; } = new CrlType("DateTime", "default", false);
        public static CrlType DateTimeOffset { get; } = new CrlType("DateTimeOffset", "default", false);
        public static CrlType TimeSpan { get; } = new CrlType("TimeSpan", "default", false);
        
        public static CrlType Decimal { get; } = new CrlType("decimal", "default", false); //
        public static CrlType Double { get; } = new CrlType("double", "0D", false);
        public static CrlType Float { get; } = new CrlType("float", "0f", false);
        
        public static CrlType ByteArray { get; } = new CrlType("byte[]", "null", false);
        public static CrlType Byte { get; } = new CrlType("byte", "0", false);
        public static CrlType Guid { get; } = new CrlType("Guid", "default", false);
        
        public static CrlType SqlGeography { get; } = new CrlType("SqlGeography", "null", false);
        
        public string Name { get; }
        public string DefaultValue { get; }
        public bool IsNullable { get; }

        private CrlType(string name, string defaultValue, bool isNullable)
        {
            Name = isNullable && !name.EndsWith('?') ? name + '?' : name;
            DefaultValue = isNullable && defaultValue != "null" ? "default" : defaultValue;
            IsNullable = isNullable;
        }

        public static CrlType FromSqlType(string sqlType, bool isNullable) => 
            FromSqlType(sqlType).WithIsNullable(isNullable);

        private static CrlType FromSqlType(string sqlType)
        {
            // https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/sql-server-data-type-mappings
            
            switch (sqlType)
            {
                case "bigint": return Long;
                case "binary": return ByteArray;
                case "bit": return Bool;
                case "char": return String;
                case "date": return DateTime;
                case "datetime": return DateTime;
                case "datetime2": return DateTime;
                case "datetimeoffset": return DateTimeOffset;
                case "decimal": return Decimal;
                case "FILESTREAM": return ByteArray;
                case "float": return Double;
                case "image": return ByteArray;
                case "int": return Int;
                case "money": return Decimal;
                case "nchar": return String;
                case "ntext": return String;
                case "numeric": return Decimal;
                case "nvarchar": return String;
                case "real": return Float;
                case "rowversion": return ByteArray;
                case "smalldatetime": return DateTime;
                case "smallint": return Short;
                case "smallmoney": return Decimal;
                case "text": return String;
                case "time": return TimeSpan;
                case "timestamp": return ByteArray;
                case "tinyint": return Byte;
                case "uniqueidentifier": return Guid;
                case "varbinary": return ByteArray;
                case "varchar": return String;
                case "xml": return String; // type "Xml" yeah ut which type?
                case "geography": return SqlGeography;
            }

            throw new ArgumentException($"parsing of sql type '{sqlType}' not yet implemented!");
        }

        private CrlType WithIsNullable(bool isNullable) => 
            new CrlType(name: Name, defaultValue: DefaultValue, isNullable: isNullable);
    }

    public class SqlName
    {
        public string Original { get; }
        public string PascalCase { get; }
        public string PascalCasePlural { get; }
        public string CamelCase { get; }
        public string CamelCasePlural { get; }

        public SqlName(string name)
        {
            Original = name;
            PascalCase = name; //TODO: dont assume pascal!
            PascalCasePlural = PascalCase + "s"; //TODO: do better
            CamelCase = PascalCase.ToCamelCase();
            CamelCasePlural = PascalCasePlural.ToCamelCase();
        }
    }

    public static class StringExtensions
    {
        public static string ToCamelCase(this string s) =>
            s[0].ToString().ToLower() + s.Substring(1);
    }
}