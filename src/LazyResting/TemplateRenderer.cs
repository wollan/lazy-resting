﻿using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace LazyResting
{
    public interface ITemplateRenderer
    {
        string Render(string template, TableInfo tableInfo, ProgramArgs args);
    }

    public class TemplateRenderer : ITemplateRenderer
    {
        public string Render(string template, TableInfo tableInfo, ProgramArgs args)
        {
            var columnConstructor = ToColumnConstructor(tableInfo);

            return template

                //columns
                .Replace(oldValue: "public PrimaryKeyX PrimaryKey { get; }", 
                         newValue: ToColumnProperties(tableInfo.Columns))
                .Replace(oldValue: "public TableXDomainModel(PrimaryKeyX primaryKey)", newValue: columnConstructor)
                .Replace(oldValue: "TableXDomainModel Create(PrimaryKeyX primaryKey)", newValue: ToColumnCreateMethod(tableInfo))
                .Replace(oldValue: "public TableXDTO(PrimaryKeyX primaryKey)", newValue: columnConstructor)
                .Replace(oldValue: "PrimaryKey = primaryKey;",
                         newValue: ToInitColumnProperties(tableInfo.Columns))
                .Replace(oldValue: "primaryKey: tableX.PrimaryKey", newValue: ToArgumentList(tableInfo.Columns))
                .Replace(oldValue: "new TableXDomainModel(primaryKey: primaryKey)", newValue: ToNewDomainModel(tableInfo.Columns))

                //namespace
                .Replace(oldValue: "ProjectX", newValue: args.ProjectName)

                //table
                .Replace(oldValue: "TableXDomainModel", newValue: tableInfo.Name.PascalCase)
                .Replace(oldValue: "TableXDomainModels", newValue: tableInfo.Name.PascalCasePlural)
                .Replace(oldValue: "TableXDTO", newValue: tableInfo.Name.PascalCase)
                .Replace(oldValue: "TableXDTOs", newValue: tableInfo.Name.PascalCasePlural)
                .Replace(oldValue: "TableX", newValue: tableInfo.Name.PascalCase)
                .Replace(oldValue: "TableXs", newValue: tableInfo.Name.PascalCasePlural)
                .Replace(oldValue: "tableX", newValue: tableInfo.Name.CamelCase)
                .Replace(oldValue: "tableXs", newValue: tableInfo.Name.CamelCasePlural)

                //primary key
                .Replace(oldValue: "primaryKey == null", 
                         newValue: $"primaryKey == {tableInfo.PrimaryKey.CrlType.DefaultValue}")
                .Replace(oldValue: "PrimaryKeyX", newValue: tableInfo.PrimaryKey.CrlType.Name)
                .Replace(oldValue: "PrimaryKeyXs", newValue: tableInfo.PrimaryKey.CrlType.Name)
                .Replace(oldValue: "PrimaryKey", newValue: tableInfo.PrimaryKey.Name.PascalCase)
                .Replace(oldValue: "PrimaryKeys", newValue: tableInfo.PrimaryKey.Name.PascalCasePlural)
                .Replace(oldValue: "primaryKeyX", newValue: tableInfo.PrimaryKey.Name.CamelCase)
                .Replace(oldValue: "primaryKeyXs", newValue: tableInfo.PrimaryKey.Name.CamelCasePlural)
                .Replace(oldValue: "primaryKey", newValue: tableInfo.PrimaryKey.Name.CamelCase)
                .Replace(oldValue: "primaryKeys", newValue: tableInfo.PrimaryKey.Name.CamelCasePlural);
        }

        private string ToArgumentList(ImmutableArray<ColumnInfo> columns) => 
            string.Join(",\n            ", columns.Select(c => $"{c.Name.CamelCase}: tableX.{c.Name.PascalCase}"));

        private string ToNewDomainModel(ImmutableArray<ColumnInfo> columns) => 
            $"new TableXDomainModel(\n                " + 
                string.Join(",\n                ", columns.Select(c => $"{c.Name.CamelCase}: {c.Name.CamelCase}")) + 
            ")";

        private string ToColumnProperties(ImmutableArray<ColumnInfo> columns) => 
            string.Join("\n        ", columns.Select(c => $"public {c.CrlType.Name} {c.Name.PascalCase} {{ get; }}"));

        private string ToColumnConstructor(TableInfo tableInfo) =>
            $"public {tableInfo.Name.PascalCase}(\n            " + 
            string.Join(",\n            ", tableInfo.Columns.Select(c => $"{c.CrlType.Name} {c.Name.CamelCase}")) + 
            ")";

        private string ToColumnCreateMethod(TableInfo tableInfo) =>
            $"{tableInfo.Name.PascalCase} Create(\n            " + 
            string.Join(",\n            ", tableInfo.Columns.Select(c => $"{c.CrlType.Name} {c.Name.CamelCase}")) + 
            ")";

        private string ToInitColumnProperties(ImmutableArray<ColumnInfo> columns) => 
            string.Join("\n            ", columns.Select(c => $"{c.Name.PascalCase} = {c.Name.CamelCase};"));
    }
}
