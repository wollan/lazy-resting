﻿using System;
using System.Threading.Tasks;

namespace LazyResting
{
    class Program
    {
        static Task Main(string[] args)
        {
            var programArgs = ProgramArgs.ParseOrExit(args);
            var app = ResolveApplication(programArgs);
            return app.Run(programArgs);
        }

        private static IApplication ResolveApplication(ProgramArgs args)
        {
            var renderer = new TemplateRenderer();
            var dbSchemaRepository = new DbSchemaRepository(connectionString: args.ConnString);
            var inspector = new DatabaseInspector(dbSchemaRepository);
            var templateProvider = new TemplateProvider();
            return new Application(renderer, inspector, templateProvider);
        }
    }
}
