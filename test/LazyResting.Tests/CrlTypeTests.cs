using Xunit;

namespace LazyResting.Tests
{
    public class CrlTypeTests
    {
        [Theory]
        [InlineData("bigint", "long")]
        [InlineData("bit", "bool")]
        [InlineData("char", "string")]
        [InlineData("date", "DateTime")]
        [InlineData("datetime", "DateTime")]
        [InlineData("datetime2", "DateTime")]
        [InlineData("decimal", "decimal")]
        [InlineData("float", "double")]
        [InlineData("geography", "SqlGeography")]
        [InlineData("int", "int")]
        [InlineData("nchar", "string")]
        [InlineData("numeric", "decimal")]
        [InlineData("nvarchar", "string")]
        [InlineData("smallint", "short")]
        [InlineData("smallmoney", "decimal")]
        [InlineData("text", "string")]
        [InlineData("time", "TimeSpan")]
        [InlineData("tinyint", "byte")]
        [InlineData("uniqueidentifier", "Guid")]
        [InlineData("varbinary", "byte[]")]
        [InlineData("varchar", "string")]
        [InlineData("xml", "string")]
        public void FromSqlType(string sqlType, string expectedCrlType)
        {
            var actual = CrlType.FromSqlType(sqlType, false);
            
            Assert.Equal(expectedCrlType, actual.Name);
        }
        
        [Theory]
        [InlineData("bit", "bool?")]
        [InlineData("nvarchar", "string?")]
        public void FromSqlTypeNullable(string sqlType, string expectedCrlType)
        {
            var actual = CrlType.FromSqlType(sqlType, true);
            
            Assert.Equal(expectedCrlType, actual.Name);
        }
    }
}